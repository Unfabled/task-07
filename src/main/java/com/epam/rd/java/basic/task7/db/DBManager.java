package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {

    }

    public List<User> findAllUsers() throws DBException {
        List<User> list = new ArrayList<>();
        try (Statement statement = getDBConnection().createStatement()) {
            ResultSet set = statement.executeQuery("SELECT id, login FROM users");
            while (set.next()) {
                int id = set.getInt("id");
                String userLogin = set.getString("login");
                User currentUser = User.createUser(userLogin);
                currentUser.setId(id);
                list.add(currentUser);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return list;
    }

    public boolean insertUser(User user) throws DBException {
        try (Statement statement = getDBConnection().createStatement()) {
            statement.executeUpdate("INSERT INTO users VALUES (DEFAULT, '" + user.getLogin() + "')");
            return true;
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            return false;
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        String[] stringArraySQL = new String[users.length];
        for (int i = 0; i < users.length; i++) {
            stringArraySQL[i] = "DELETE FROM users WHERE login='" +
                    users[i].getLogin() + "'";
        }
        try (Statement statement = getDBConnection().createStatement()) {
            for (String stringSQL : stringArraySQL) {
                statement.executeUpdate(stringSQL);
            }
            return true;
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        try (Statement statement = getDBConnection().createStatement()) {
            ResultSet set = statement.executeQuery("SELECT id, login FROM users");
            while (set.next()) {
                int id = set.getInt("id");
                String loginUser = set.getString("login");
                if (loginUser.equals(login)) {
                    User user = User.createUser(loginUser);
                    user.setId(id);
                    return user;
                }
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        System.out.println("User is not found. Return value - null");
        return null;
    }

    public Team getTeam(String name) throws DBException {
        try (Statement statement = getDBConnection().createStatement()) {
            ResultSet set = statement.executeQuery("SELECT id, name FROM teams");
            while (set.next()) {
                int id = set.getInt("id");
                String nameTeam = set.getString("name");
                if (nameTeam.equals(name)) {
                    Team team = Team.createTeam(nameTeam);
                    team.setId(id);
                    return team;
                }
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        System.out.println("Team is not found. Return value - null");
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> list = new ArrayList<>();
        try (Statement statement = getDBConnection().createStatement()) {
            ResultSet set = statement.executeQuery("SELECT id, name FROM teams");
            while (set.next()) {
                int idTeam = set.getInt("id");
                String teamName = set.getString("name");
                Team currentTeam = Team.createTeam(teamName);
                currentTeam.setId(idTeam);
                list.add(currentTeam);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return list;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Statement statement = getDBConnection().createStatement()) {
            statement.executeUpdate("INSERT INTO teams VALUES (DEFAULT, '" + team.getName() + "')");
            Team team1 = getTeam(team.getName());
            team.setId(team1.getId());
            return true;
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            return false;
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        User updateIdUser = getUser(user.getLogin());
        for (int i = 0; i < teams.length; i++) {
            Team IdTeam = getTeam(teams[i].getName());
            teams[i] = IdTeam;
        }

        String[] stringArraySQL = new String[teams.length];
        for (int i = 0; i < teams.length; i++) {
            stringArraySQL[i] = "INSERT INTO users_teams (user_id, team_id) VALUES (" +
                    updateIdUser.getId() + ", " + teams[i].getId() + ")";
        }
        Connection con = null;
        try {
            con = getDBConnection();
            Statement statement = con.createStatement();
            con.setAutoCommit(false);
            for (String singleSQL : stringArraySQL) {
                statement.executeUpdate(singleSQL);
            }
            con.commit();
            return true;
        } catch (SQLException throwable) {
            try {
                con.rollback();
                throw new DBException("Transaction is being rolled back", throwable);
            } catch (SQLException e) {
                System.err.print("Error during rollback");
            }
        } finally {
            try {
                if (con != null) con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        User updateIdUser = getUser(user.getLogin());
        String stringSQL = "SELECT team_id FROM users_teams WHERE user_id = " + updateIdUser.getId();
        List<Integer> listUserTeamId = new ArrayList<>();
        try (Statement statement = getDBConnection().createStatement()) {
            ResultSet set = statement.executeQuery(stringSQL);
            while (set.next()) {
                Integer teamId = set.getInt("team_id");
                listUserTeamId.add(teamId);
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        List<Team> result = findAllTeams();
        result.removeIf(team -> !listUserTeamId.contains(team.getId()));
        return result;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String stringSQL = "DELETE FROM teams WHERE name = '" + team.getName() + "'";
        try (Statement statement = getDBConnection().createStatement()) {
            statement.executeUpdate(stringSQL);
            return true;
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        String stringSQL = "UPDATE teams SET name = '" +
                team.getName() + "' WHERE id = " +
                team.getId();
        try (Statement statement = getDBConnection().createStatement()) {
            statement.executeUpdate(stringSQL);
            return true;
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return false;
    }

    private static Connection getDBConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(getURL("app.properties"));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static String getURL(String fileName) {
        Properties properties = new Properties();
        try (InputStream input = Files.newInputStream(Path.of(fileName))) {
            properties.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("connection.url");
    }

}
